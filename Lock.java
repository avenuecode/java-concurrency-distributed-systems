class Lock<T> {

    private final T state;
    // Implement any other lock metadata you wish

    public Lock(T state) {
        this.state = state;
    }

    public T state() {
        return state;
    }
    
}
